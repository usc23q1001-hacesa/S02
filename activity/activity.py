# 2.) Create 5 viarlabes and output them in the command prompt.
name = "Hans Adey Cesa"
age = 22
occupation = "Student"
movie = "Double Patty"
rating = 100.00

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for the movie {movie} is {rating}%.")

# 3.) Create 3 variables, num1, num2, num3
num1 = 2
num2 = 3
num3 = 4

# a. Get the product of num1 and num2
print(num1 * num2)

# b. Check if num1 is less than num3
print(num1 < num3)

# c. Add the value of num3 to num2
num2 += num3
print(num2)
